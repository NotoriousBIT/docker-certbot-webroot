FROM alpine:3.12

RUN apk add --no-cache --update certbot lighttpd bash
RUN rm -rf /var/cache/apk/*

RUN mkdir /scripts

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
ENTRYPOINT ["docker-entrypoint.sh"]