#!/bin/bash

# lighttpd
ln -sf /dev/stderr /var/log/lighttpd/error.log
ln -sf /dev/stdout /var/log/lighttpd/access.log
chown lighttpd:wheel /var/log/lighttpd/*

lighttpd -f /etc/lighttpd/lighttpd.conf

# cron
chmod +x /scripts/*.sh

for f in /etc/cron.d/* ;
  do crontab "$f" ;
done

crond -f