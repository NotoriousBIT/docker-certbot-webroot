#!/bin/bash
certbot certonly \
        --webroot \
        -w /var/www/localhost/htdocs/ \
        -d www.example.tld,example.tld \
        -n \
        --agree-tos \
        -m admin@example.tld \
        --expand